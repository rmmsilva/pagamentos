package br.com.itau.pagamento.services;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import br.com.itau.pagamento.models.Pagamento;
import br.com.itau.pagamento.models.Pedido;

@Service
public class PagamentoService {

	@Autowired
	private KafkaTemplate<String, Object> template;

	public Boolean efetuarPagamento(Pagamento pagamento) {
		Random rand = new Random();

		int randomNum = rand.nextInt((100 - 1) + 1) + 1;

		if (pagamento != null && pagamento.getValor() > 0 && randomNum >= 30) {
			pagamento.setId(randomNum);
			return true;
		}

		return false;
	}

	public void analisarPagamento(Pedido pedido) {
		int possibilidade = new Random(System.currentTimeMillis())
				.nextInt(100) + 1;
		boolean aprovado = possibilidade > 50;

		if (aprovado) {
			pedido.setStatus("Pagamento aprovado");
		} else {
			pedido.setStatus("Pagamento não realizado");
		}

		template.send("pedidos-analisados", pedido);
	}
}
