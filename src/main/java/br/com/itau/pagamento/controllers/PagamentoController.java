package br.com.itau.pagamento.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.pagamento.models.Pagamento;
import br.com.itau.pagamento.services.PagamentoService;

@RestController
public class PagamentoController {

	@Autowired
	PagamentoService pagamentoService;
	
	@RequestMapping(value = "/", method = RequestMethod.POST, produces = {"application/JSON"})
	public @ResponseBody ResponseEntity<Pagamento> efetuarPagamento(@RequestBody Pagamento pagamento)
	{	
		Boolean resultadoOperacaoPagamento = pagamentoService.efetuarPagamento(pagamento);
		
		return new ResponseEntity<Pagamento>(pagamento
				              , resultadoOperacaoPagamento ? HttpStatus.OK : HttpStatus.NOT_ACCEPTABLE);
	}
	
}