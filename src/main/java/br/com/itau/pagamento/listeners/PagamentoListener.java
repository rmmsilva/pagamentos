package br.com.itau.pagamento.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import br.com.itau.pagamento.models.Pedido;
import br.com.itau.pagamento.services.PagamentoService;

@Component
public class PagamentoListener {

	@Autowired
	PagamentoService pagamentoService;

	@KafkaListener(topics = "analise-pedidos", groupId = "pagamentos", properties = "auto.offset.reset=earliest")
	public void analisarPedido(Pedido pedido) {
		pagamentoService.analisarPagamento(pedido);
	}
}
